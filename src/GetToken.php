<?php
namespace Larabookir\Gateway;

use Exception;
use GuzzleHttp\Client;

class GetToken
{
    protected $client;
    public function __construct()
    {
        $this->client=new Client();
    }
    public function samanGenerateToken($method,$options)
    {
        try {
            $response=$this->client->request($method,config('gateway.saman.generate_token_url'),$options);
            return $response->getBody()->getContents();
        } catch (Exception $e) {
            throw new Exception($e);
        }

    }

}