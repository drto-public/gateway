<?php


namespace Larabookir\Gateway\Mellat;


use GuzzleHttp\Client;

//@todo: not completed yet, maybe next time.
trait MellatProxy
{
    protected function getWsdlViaProxy(string $wsdlUrl)
    {
        if (empty($wsdlUrl)) {
            throw new \Exception('WSDL url not provided');
        }
        $parsedUrl = parse_url($wsdlUrl);
        $cachedFile = storage_path(sprintf('%s.%s.xml', $parsedUrl['host'], $parsedUrl['query'] ?? 'wsdl'));

        if (file_exists($cachedFile) && time() - filemtime($cachedFile) > 6 * 3600 && filesize($cachedFile) > 100) {
            return $cachedFile;
        }

        $client = new Client();
        $options = [
            'proxy' => [
                'https' => sprintf('%s:://%s:%s@%s:%s',
                    config('gateway.mellat.proxy.proxy_type'),
                    config('gateway.mellat.proxy.proxy_login'),
                    config('gateway.mellat.proxy.proxy_password'),
                    config('gateway.mellat.proxy.proxy_host'),
                    config('gateway.mellat.proxy.proxy_port'),
                )
            ]
        ];
        try {
            $result = $client->request('get', $wsdlUrl, $options);
        }catch (\Exception $ex) {
            throw $ex;
        }
        $data = $result->getBody()->getContents();
        $dom = new \DOMDocument();
        $dom->preserveWhiteSpace = true;
        $dom->loadXML($data);
        $dom->save($cachedFile);
        return $cachedFile;
    }
}