<?php

namespace Larabookir\Gateway\Saman;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Larabookir\Gateway\CurlSoapClient;
use SoapClient;
use Larabookir\Gateway\PortAbstract;
use Larabookir\Gateway\PortInterface;

class Saman extends PortAbstract implements PortInterface
{

    /**
     * {@inheritdoc}
     */
    public function set($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function ready()
    {
        $this->newTransaction();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function redirect()
    {

        return [
            'amount' => $this->amount,
            'merchant' =>  $this->config->get('gateway.saman.merchant'),
            'resNum' => $this->transactionId(),
            'url' => $this->config->get('gateway.saman.is_sandbox') ?
				$this->config->get('gateway.saman.dev_payment_form') :
				$this->config->get('gateway.saman.payment_form'),
            'callBackUrl' => $this->getCallback()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function verify($transaction)
    {
        parent::verify($transaction);

        $this->userPayment();
        $this->verifyPayment();

        return $this;
    }

    function getServerUrl() {
    	return $this->config->get('gateway.saman.is_sandbox') ?
			$this->config->get('gateway.saman.dev_wsl_url') :
			$this->config->get('gateway.saman.wsl_url');

	}
    /**
     * Sets callback url
     * @param $url
     */
    function setCallback($url)
    {
        $this->callbackUrl = $url;
        return $this;
    }

    /**
     * Gets callback url
     * @return string
     */
    function getCallback()
    {

        $now = Carbon::now();
        $url = $this->makeCallback($this->callbackUrl, [
            'transaction_id' => $this->transactionId()
            , 'datetime' => $now->format('Y-m-d H:i:s')
        ]);

        return $url;
    }


    /**
     * Check user payment
     *
     * @return bool
     *
     * @throws SamanException
     */
    protected function userPayment()
    {
        $this->refId = Input::get('RefNum');
        $this->trackingCode = Input::get('ResNum');
        $payRequestRes = Input::get('State');
        $this->cardNumber = Input::get('SecurePan');
        $payRequestResCode = Input::get('StateCode');

        if ($payRequestRes == 'OK') {
            return true;
        }

        $this->transactionFailed();
        $this->newLog($payRequestResCode, @SamanException::$errors[$payRequestRes]);
        throw new SamanException($payRequestRes);
    }


    /**
     * Verify user payment from bank server
     *
     * @return bool
     *
     * @throws SamanException
     * @throws SoapFault
     */
    protected function verifyPayment()
    {
        $fields = array(
            "merchantID" => $this->config->get('gateway.saman.merchant'),
            "RefNum" => $this->refId,
            "password" => $this->config->get('gateway.saman.password'),
        );


        try {
            $soap = new CurlSoapClient($this->getServerUrl(), $this->config->get('gateway.saman.proxy'));
            $response = $soap->VerifyTransaction($fields["RefNum"], $fields["merchantID"]);

        } catch (\SoapFault $e) {
            $this->transactionFailed();
            $this->newLog('SoapFault', $e->getMessage());
            throw $e;
        }

        $response = intval($response);

        if ($response != $this->amount) {

            //Reverse Transaction
            if ($response > 0) {
                try {
                    $soap = new CurlSoapClient($this->getServerUrl(), $this->config->get('gateway.saman.proxy'));
                    $response = $soap->ReverseTransaction($fields["RefNum"], $fields["merchantID"], $fields["password"], $response);

                } catch (\SoapFault $e) {
                    $this->transactionFailed();
                    $this->newLog('SoapFault', $e->getMessage());
                    throw $e;
                }
            }

            //
            $this->transactionFailed();
            $this->newLog($response, SamanException::$errors[$response]);
            throw new SamanException($response);
        }


        $this->transactionSucceed();

        return true;
    }


}